<section id="informazioni_personali">
    <h1>Informazioni personali</h1>
    <dl>
        <dt>
            Nome e Cognome
        </dt>
        <dd>
            <p>
                <span id="nome">Lingyong</span> <span id="cognome">Sun</span>
            </p>
        </dd>
        <dt>
            Contatti social
        </dt>
        <dd>
            <dl id="contatti_social">
                <dt>
                    Twitter
                </dt>
                <dd>
                    <a href="https://twitter.com/lysz210" target="blank">Lysz210</a>
                </dd>
                <dt>
                    LinkedIn
                </dt>
                <dd>
                    <a href="https://it.linkedin.com/in/lysz210" target="blank">Lingyong Sun</a>
                </dd>
                <dt>
                    Github
                </dt>
                <dd>
                    <a href="https://github.com/lysz210" alt="ling al profilo su Github" target="blank">Lysz210</a>
                </dd>
                <dt>
                    Bitbucket
                </dt>
                <dd>
                    <a href="https://bitbucket.org/lysz210/" alt="ling al profilo su Bitbucket" target="bland">Lysz210</a>
                </dd>
                <dt>
                    Skype
                </dt>
                <dd>
                    lysz210
                </dd>
                <dt>
                    Facebook
                </dt>
                <dd>
                    <a href="https://www.facebook.com/lysZ210" alt="link alla pagina su facebook" target="blank">Lysz210</a>
                </dd>
                <dt class="print_only">
                    Curriculum online
                </dt>
                <dd class="print_only">
                    <a href="{{Request::url()}}" target="blank">Curriculum vitae online</a>
                </dd>
            </dl>
        </dd>
    </dl>
</section>
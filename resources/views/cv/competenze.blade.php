<section id="competenze_personali">
    <h1>Competenze personali</h1>

    <dl>

        <dt>
            Lingua madre
        </dt>
        <dd>
            Italiano
        </dd>

        <dt>
            Altre lingue
        </dt>
        <dd>
            <table>
                <thead>
                    <tr>
                        <th> </th>
                        <th colspan="2">COMPRENSIONE</th>
                        <th colspan="2">PARLATO</th>
                        <th>PRODUZIONE SCRITTA</th>
                    </tr>
                    <tr>
                        <th> </th>
                        <th>Ascolto</th>
                        <th>Lettura</th>
                        <th>Interazione</th>
                        <th>Produzione orale</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Inglese</td>
                        <td>B2</td>
                        <td>B2</td>
                        <td>B2</td>
                        <td>B2</td>
                        <td>B2</td>
                    </tr>
                    <tr>
                        <td>Francese</td>
                        <td>A1</td>
                        <td>A1</td>
                        <td>A1</td>
                        <td>A1</td>
                        <td>A1</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            Livelli: A1/2 Livello base - B1/2 Livello intermedio - C1/2 Livello avanzato
                            <br />
                            Quadro Comune Europeo di Riferimento delle Lingue
                        </td>
                    </tr>
                </tfoot>
            </table>
        </dd>

        <dt>
            Competenze informatiche
        </dt>
        <dd>
            <ul>
                <li>
                    Database NoSQL
                    <ul>
                        <li>CouchDB</li>
                        <li>mongodb</li>
                    </ul>
                </li>
                <li>
                    Programmazione in javascript
                    <ul>
                        <li>
                            Lato client
                            <ul>
                                <li>Uso di librerie base. Jquery</li>
                                <li>Uso di framework avanzati. Angular, principalmente Vuejs</li>
                                <li>Comunicazione con Socket.io, ajax, rest API</li>
                                <li>Pouchdb per l'uso di database in locate e in remote</li>
                            </ul>
                        </li>
                        <li>
                            Lato server(Nodejs)
                            <ul>
                                <li>Creazione base di server http</li>
                                <li>Creazione di script generici per cron</li>
                                <li>Gestione connessione a DBMS</li>
                                <li>Creazione librerie</li>
                            </ul>
                        </li>
                    </ul>		
                </li>
                <li>
                    Programmazione in PHP
                    <ul>
                        <li>Uso di composer, compresa la preparazione di repository</li>
                        <li>Laravel ed altre librerie</li>
                    </ul>
                </li>
                <li>
                    Creazione di siti in HTML/CSS
                </li>
                <li>Programmazione sass per la compilazione di css</li>
                <li>
                    Programmazione base in java
                </li>
                <li>
                    Programmazione base in python
                </li>
                <li>
                    Conoscenza basi di Django
                </li>
                <li>
                    Conoscenza basilare di Assembler
                </li>
                <li>
                    Creazione e gestioni basi di dati in PostgreSQL/MySQL
                </li>
                <li>
                    Programmazione base di linguaggio funzionale (CaML)
                </li>
                <li>
                    Programmazione base di C
                </li>
                <li>
                    Conoscenze basilari di R
                </li>
                <li>
                    Buona padronanza degli strumenti Microsoft Office
                </li>
            </ul>
        </dd>

        <dt>
            Patende di guida
        </dt>
        <dd>
            B
        </dd>
    </dl>
</section>
<section id="istruzione e formazione">
    <h1>Istruzione e formazione</h1>
    <dl>
        <dt>
            Settembre 2013 – in corso...
        </dt>
        <dd>
            <a href="http://www.unive.it" alt="link all'universita ca foscari" target="blank">Ca' Foscari</a>
            <p>
                <a href="http://www.unive.it/pag/3/" alt="link al corso di laurea triennale dell'università Ca Foscari" target="blank">Corso di laurea triennale in informatica</a>
            </p>
        </dd>

        <dt>
            Ottobre 2009 – Giugno 2013
        </dt>
        <dd>
            <a href="http://www.zuccante.it" alt="link all'istituto Zuccante di mestre" target="blank">Itis C. Zuccante</a>
            <p>
                <a href="http://scuoleserali.altervista.org/blog/" alt="link al corso serale di informatica dell&apos; istituto zuccante" target="blank">Corso serale di Informatica</a>
            </p>
        </dd>

        <dt>
            2008 (seconda settimana di Febbraio)
        </dt>
        <dd>
            <a href="https://www.facebook.com/PBSAcademy-29696457373/timeline/" alt="ling alla pagina facebook di PBS Academy" target="blank">PBS Accademy</a>
            <p>
                <a href="https://www.facebook.com/PBSAcademy-29696457373/timeline/" alt="ling alla pagina facebook dei corsi di PBS Academy" target="blank">Corso base per la miscelazione di cocktails</a>
            </p>
        </dd>
    </dl>
</section>
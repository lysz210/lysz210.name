<section id="esperienze_professionali">
    <h1>Esperienze professionali</h1>
    <dl>
        <dt>Occupazione attuale da Gennaio 2019</dt>
        <dd>
            <h2>Consulente informatico cliente principale <a href="https://www.reply.com/it/" alt="link al sito Reply" target="blank">Reply</a> di Silea</h2>
            <p>
                <a href="https://www.telematicainformatica.it/" alt="link al sito Telematica Informatica Srl" target="blank">Telematica Informatica Srl</a>,
                Corso Rosselli 240, Torino 10141
            </p>
            <ul>
                <li>Sviluppo front-end in Java</li>
                <li>Tecnologie principali: Spring, Primefaces, sass, JBoss</li>
                <li>Versioning con GitLab</li>
            </ul>
            <p class="settore">
                Informatica
            </p>
        </dd>
        <dt>
            Dal Agosto 2016 al Dicembre 2018
        </dt>
        <dd>
            <h2>Web developer</h2>
            <p>
                <a href="http://www.studioitaliatech.com/" alt="link al sito Studio Italia Tech Srl" target="blank">Studio Italia Tech s.r.l.</a>,
                Via Pialoi 30 - 30020 Marcon - Venezia (Italia)
            </p>
            <ul>
                <li>
                    Sviluppo back-end, principalmente in php
                </li>
                <li>
                    Sviluppo front-end
                </li>
                <li>
                    Sviluppo applicazioni con Electron
                </li>
                <li>
                    Aumotazione di alcuni processi ripetitivi emulabili attraverso script con linguaggi di programmazione
                </li>
            </ul>
            <p class="settore">
                Informatica
            </p>
        </dd>
        <dt>
            Dal 2004 al 21 Giugno 2013
        </dt>
        <dd>
            <h2>Pizzaiolo</h2>
            <p>
                <a href="http://www.flunch.it/" alt="link al sito ufficiale flunch italia" target="blank">Flunch Italia s.r.l.</a>, via Don Tosatto 1, Mestre(Venezia)
            </p>
            <ul>
                <li>
                    Produzione e vendita di pizze al trancio
                </li>
                <li>
                    Mantenimento dell'ordine magazzino alimentare
                </li>
            </ul>
            <p class="settore">
                Ristorazione self-service
            </p>
        </dd>

        <dt>
            Dal 2002 al 2004
        </dt>
        <dd>
            <h2>Adetto generico</h2>
            <p>
                <a href="http://www.caribesrl.com/index.html" alt="link al sito ufficiale dell'azienda Caribe srl" target="blank">Caribe s.r.l.</a>, McDonald's in franchising, calle Larga San Marco Venezia
            </p>
            <ul>
                <li>
                    Banconiere
                </li>
                <li>
                    Produzione panini
                </li>
                <li>
                    inizio formazione nel ruolo manageriale
                </li>
            </ul>
            <p class="settore">
                Ristorazione fast-food
            </p>
        </dd>

        <dt>
            Dal 1996 al 2002
        </dt>
        <dd>
            <h2>Lavoro in azienda familiare</h2>
            <p>
                Friendshop of the world (Azienda familiare)
            </p>
            <ul>
                <li>
                    Vendita al dettaglio di accessori di abbigliamento (principalmente borse in pelle)
                </li>
                <li>
                    Produzione di borse in pelle
                </li>
                <li>
                    Ricamo su magliette
                </li>
            </ul>
            <p class="settore">
                Vendita al dettaglio di prodotti non alimentari, lavorazione artigianale di pellame e

                abbigliamento.
            </p>
        </dd>

    </dl>
</section>